# purepython-udp-through-ssh

A simple, pure python implementation of UDP through TCP.
Designed for servers with stock Ubuntu without internet access.
Only Python3 required.